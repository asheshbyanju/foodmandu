class OrdersController < ApplicationController
  
  #before_action :authenticate_user!, except: [:index, :show]
  #before_action :set_restaurant, only: [:edit, :update, :destroy]
  #after_action :verify_authorized, except: [:index, :show] 
  #after_action :verify_policy_scoped, only: :index

  def index
      
  end

  def new
    #@restaurant = Restaurant.find(params[:id])
    @item = Menu.find(params[:menu_id])
    @order = @item.orders.build
  end

  def show
    @order = Order.all
   
  end

  def create
    #binding.pry
    #@restaurant = Restaurant.find(params[:id])
    @item = Menu.find(params[:menu_id])

    @order = @item.orders.build order_params
  
     #authorize @user
    if @order.save
    flash[:notice] = "Order Is Success"
      redirect_to(restaurants_path)
    else  
      render('new')
    end
  end

  def edit
  end

  def update
  
  end


  def delete

  end

  def menu_name
  @item = Menu.find(params[:menu_id])
  @name = @item.item

  end
  

  def order_params
  params.require(:order).permit(:menu_id,:quantity)

  end

end
