class RestaurantsController < ApplicationController
  
  before_action :authenticate_user!, except: [:index, :show]
  #before_action :set_restaurant, only: [:edit, :update, :destroy]
  #after_action :verify_authorized, except: [:index, :show] 
  #after_action :verify_policy_scoped, only: :index

  def index
   #@restaurants = policy_scope(Restaurant).reverse
     #  if params[:query].present?
  #   @restaurants = Restaurant.search(params[:query], load: true)
  # # else
      search = params[:query].present? ? params[:query] : nil
      @restaurants = if search
      Restaurant.search(search, fields: [:location, :name, :cuisine] )
  
    else 
   @restaurants = Restaurant.paginate(:page => params[:page], :per_page => 2).order(:name)
  end
   @menus = Menu.all
   #end
    #@restaurants = Restaurant.all
  end

  def new
    #binding.pry

    @restaurant = current_user.restaurants.new
    
    #@restaurant = @user.restaurants.new
    authorize @restaurant
    #authorize @user
  end

  def create
    @restaurant = current_user.restaurants.create(restaurant_params)
    #@restaurant = Restaurant.create(restaurant_params)
    logger.debug @restaurant.errors.full_messages
    authorize @restaurant
    #authorize @user
    
    if @restaurant.save
      flash[:notice] = "Restaurant Added Successfully"
      redirect_to(restaurants_path)
    else 
      render('new')
    end
  end
  
  def show
    #binding.pry
    @restaurant = Restaurant.find(params[:id])
    @menu = @restaurant.menus
  end
  
  def edit
    @restaurant = Restaurant.find(params[:id])
    authorize @restaurant
  end

  def update
    @restaurant = Restaurant.find(params[:id])

    authorize @restaurant
    if @restaurant.update_attributes(restaurant_params)

    flash[:notice] = "Restaurant Updated Successfully"
    #redirect_to(pages_path)
    redirect_to(restaurants_path(@restaurant))
    else
      render('edit')
    end
  end


  def delete
    @restaurant = Restaurant.find(params[:id])
    authorize @restaurant
    @restaurant.destroy
    flash[:notice] = "Restaurant Deleted Successfully"
    redirect_to(restaurants_path)

  end

  private
  def set_restaurant
    @resturant = Restaurant.find(params[:id])
    #authorize @restaurant
  end

  def restaurant_params
  params.require(:restaurant).permit(:name, :location, :cuisine, :service_charge, :vat, :contact,:user_id, :image_url,:utf8, :authenticity_token)

  end



end
