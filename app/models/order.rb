class Order < ApplicationRecord
belongs_to :menu


validates :quantity, presence: true, numericality: true

end
