class Restaurant < ApplicationRecord
	belongs_to :user
	has_many :menus
	

	validates :name, :location, presence: true
	
	searchkick word_middle: [:name, :location]

	def search_data
		{
			name: name,
			location: location,
			cuisine: cuisine
		}
	end

	def remote_image_url
		if image_url.present?
			image_url
		else
			"https://loremflickr.com/320/240/#{CGI.escape name}"
		end
	end


end
