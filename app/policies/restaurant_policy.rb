 class RestaurantPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      #scope.where(role: :admin).or(
      if @user.try(:admin?) 
        scope.all
      else
        scope.where(user_id: @user.try(:id)) 
     # scope.
    end
      #)
    end
  end

  def new?
 #@user.admin? 
 @user.try(:admin?) 
 #@user.nil?
 #user_is_owner_of_restaurant?
  end

 def create? ; @user.admin? ; 
 end

  def update?
  @user.admin?
  end

  def delete?
  @user.admin?
  end

  def user_is_owner_of_restaurant?
  	@user == @user.admin?
  end
end
