class CreateMenus < ActiveRecord::Migration[5.1]
  def change
    create_table :menus do |t|
    	t.string "item"
    	t.float "price"
    	t.integer  "restaurant_id"

      t.timestamps
    end
  end
end
