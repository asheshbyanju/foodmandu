class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
    	t.integer "menu_id"
    	t.integer "quantity"
    	t.integer  "restaurant_id"

      t.timestamps
    end
  end
end
