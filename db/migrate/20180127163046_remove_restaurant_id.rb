class RemoveRestaurantId < ActiveRecord::Migration[5.1]
  def change
  	remove_column("orders", "restaurant_id")
  end
end
